<?php

namespace L4p1n\Form\Element;

use L4p1n\Form\Form;

/**
 * Class BaseElement
 * @package L4p1n\Form\Element
 */
abstract class BaseElement {

	/**
	 *
	 */
	const FORM_ELEMENT_RENDER_LABEL = 'label';
	/**
	 *
	 */
	const FORM_ELEMENT_RENDER_INPUT = 'input';

	/**
	 * @var array
	 */
	protected $attributes = [];
	/**
	 * @var
	 */
	protected $label;
	/**
	 * @var
	 */
	protected $prefix;
	/**
	 * @var
	 */
	protected $postfix;
	/**
	 * @var
	 */
	protected $error;

	/**
	 * @var null
	 */
	protected $type = null;

	/**
	 * @var array
	 */
	private $errors;

	/**
	 * @param $name
	 * @param $label
	 */
	public function __construct($name, $label){
		if(!empty($this->type)){
			$this->attribute('type', $this->type);
		}

		$this->attribute('name', $name);
		$this->label = $label;
	}

	/**
	 * @param $name
	 * @param $value
	 * @return $this
	 */
	public function attribute($name, $value){
		$this->attributes[$name] = $value;
		return $this;
	}

	// @codeCoverageIgnoreStart

	/**
	 * @param $value
	 * @return BaseElement
	 */
	public function value($value){
		return $this->attribute('value', $value);
	}

	/**
	 * @param $placeholder
	 * @return BaseElement
	 */
	public function placeholder($placeholder){
		return $this->attribute('placeholder', $placeholder);
	}

	/**
	 * @param $class
	 * @return BaseElement
	 */
	public function f_class($class){
		if(isset($this->attributes['class'])){
			$class = $class . ' ' . $this->attributes['class'];
		}

		return $this->attribute('class', $class);
	}

	/**
	 * @return $this
	 */
	public function required(){
		return $this->attribute('required', 'required');
	}

	/**
	 * @param int $length
	 * @return $this
	 */
	public function maxLength($length){
		return $this->attribute('max_length', $length);
	}

	/**
	 * @return $this
	 */
	public function noAutocomplete(){
		return $this->attribute('autocomplete', 'off');
	}

	public function autofocus(){
		return $this->attribute('autofocus', 'autofocus');
	}

	public function readonly(){
		return $this->attribute('readonly', 'readonly');
	}

	public function pattern($regexp){
		return $this->attribute('pattern', $regexp);
	}

	public function noSpellcheck(){
		return $this->attribute('spellcheck', 'off');
	}

	/**
	 * @param $id
	 * @return $this
	 */
	public function id($id){
		return $this->attribute('id', $id);
	}

	// @codeCoverageIgnoreEnd
	/**
	 * @param $prefix
	 * @return $this
	 */
	public function prefix($prefix){
		$this->prefix = $prefix;
		return $this;
	}

	/**
	 * @param $postfix
	 * @return $this
	 */
	public function postfix($postfix){
		$this->postfix = $postfix;
		return $this;
	}

	/**
	 * @param $mode
	 * @param $what
	 * @return string
	 */
	public abstract function render($mode, $what);

	// @codeCoverageIgnoreStart
	/**
	 * @return mixed
	 */
	public function getPrefix(){
		return $this->prefix;
	}
	/**
	 * @return mixed
	 */
	public function getPostfix(){
		return $this->postfix;
	}

	/**
	 * @return mixed
	 */
	public function getError(){
		return $this->error;
	}

	// @codeCoverageIgnoreEnd
	/**
	 * @param $mode
	 * @return null|string
	 */
	public function buildPrefix($mode){
		// TODO: Foundation mode
		if($mode == Form::FORM_BUILDING_MODE_BOOTSTRAP){
			return '<div class="input-group-addon">'. $this->prefix .'</div>';
		}

		return null;
	}

	/**
	 * @param $mode
	 * @return null|string
	 */
	public function buildPostfix($mode){
		if($mode == Form::FORM_BUILDING_MODE_BOOTSTRAP){
			return '<div class="input-group-addon">'. $this->postfix .'</div>';
		}

		return null;
	}

	/**
	 * @param $error
	 */
	public function setError($error){
		$this->error = $error;
	}

	/**
	 * @return bool
	 */
	public function hasError(){
		return !empty($this->error);
	}

	/**
	 * @return mixed
	 */
	public function getName(){
		return $this->attributes['name'];
	}

	/**
	 * @return string
	 */
	protected function buildAttr(){
		$o = '';
		foreach($this->attributes as $attribute => $value){
			$o .= " $attribute=\"$value\"";
		}

		return ltrim($o);
	}
}
