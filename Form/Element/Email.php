<?php

namespace L4p1n\Form\Element;


use L4p1n\Form\Form;
use utilphp\util;

/**
 * Class Email
 * @package L4p1n\Form\Element
 */
class Email extends BaseElement{
	/**
	 * @var string
	 */
	protected $type = 'email';

	/**
	 * @param $mode
	 * @param $what
	 * @return null|string
	 */
	public function render($mode, $what){
		if($what == self::FORM_ELEMENT_RENDER_LABEL){
			$o = '<label';
			if($mode == Form::FORM_BUILDING_MODE_PLAIN || $mode == Form::FORM_BUILDING_MODE_FOUNDATION){
				$o .= '>' . $this->label;
			}elseif($mode == Form::FORM_BUILDING_MODE_BOOTSTRAP){
				$o .= ' class="control-label">' . $this->label;
			}

			$o .= '</label>';
			return $o;
		}

		if($what == self::FORM_ELEMENT_RENDER_INPUT){
			if(
				$mode == Form::FORM_BUILDING_MODE_BOOTSTRAP
				&& (!isset($this->attributes['class']) || !util::str_contains($this->attributes['class'], 'form-control'))
			){
				$this->f_class('form-control');
			}

			$o = '<input ' . $this->buildAttr() . '/>';
			return $o;
		}

		return null;
	}
}