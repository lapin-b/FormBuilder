<?php

namespace L4p1n\Form\Element;


class Hidden extends BaseElement{

	public $type = 'hidden';

	/**
	 * @param $mode
	 * @param $what
	 * @return string
	 */
	public function render($mode, $what){

		$this->value($this->label);

		if($what == self::FORM_ELEMENT_RENDER_INPUT){
			$o = '<input ' . $this->buildAttr() . '/>';
			return $o;
		}

		return null;
	}
}