<?php

namespace L4p1n\Form\Element;


use L4p1n\Form\Form;
use utilphp\util;

class Select extends BaseElement{

	protected $type = '';
	protected $options = [];

	public function render($mode, $what){
		if($what == self::FORM_ELEMENT_RENDER_LABEL){
			$o = '<label';
			if($mode == Form::FORM_BUILDING_MODE_PLAIN || $mode == Form::FORM_BUILDING_MODE_FOUNDATION){
				$o .= '>' . $this->label;
			}elseif($mode == Form::FORM_BUILDING_MODE_BOOTSTRAP){
				$o .= ' class="control-label">' . $this->label;
			}

			$o .= '</label>';
			return $o;
		}

		if($what == self::FORM_ELEMENT_RENDER_INPUT){
			if(
				$mode == Form::FORM_BUILDING_MODE_BOOTSTRAP
				&& (!isset($this->attributes['class']) || !util::str_contains($this->attributes['class'], 'form-control'))
			){
				$this->f_class('form-control');
			}

			$o = '<select ' . $this->buildAttr() . '>';
			if(empty($this->options)){
				throw new \InvalidArgumentException('$options is empty');
			}

			foreach($this->options as $key => $option){
				$o .= '<option' . (!empty($key) ? ' value="' . $key . '"' : '') . '>' . $option .'</option>';
			}

			$o .= '</select>';
			return $o;
		}

		return null;
	}

	public function options(array $options){
		$this->options = $options;
		return $this;
	}
}