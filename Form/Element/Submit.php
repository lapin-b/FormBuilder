<?php
namespace L4p1n\Form\Element;

use L4p1n\Form\Form;

class Submit extends BaseElement{
    protected $ctype = 'primary';

	public function render($mode, $what){
		if($what == BaseElement::FORM_ELEMENT_RENDER_INPUT){
			if($mode == Form::FORM_BUILDING_MODE_BOOTSTRAP){
				$this->f_class('btn btn-' . $this->ctype);

				$o = '<button ';
				$o .= $this->buildAttr();
				$o .= '>';
				$o .= $this->label;
				$o .= '</button>';

				return $o;
			}elseif($mode == Form::FORM_BUILDING_MODE_FOUNDATION){
				// TODO: Foundation support
			}else if($mode == Form::FORM_BUILDING_MODE_PLAIN){
				$this->value($this->label);
				return '<input type="submit" ' . $this->buildAttr() . '/>';
			}
		}

		return null;
    }

	// @codeCoverageIgnoreStart
	public function setType($type){
		$this->ctype = $type;
	}
	// @codeCoverageIgnoreEnd
}
