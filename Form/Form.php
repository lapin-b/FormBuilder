<?php

namespace L4p1n\Form;

use L4p1n\Form\Element\BaseElement;
use L4p1n\Form\Element\Email;
use L4p1n\Form\Element\Hidden;
use L4p1n\Form\Element\Number;
use L4p1n\Form\Element\Password;
use L4p1n\Form\Element\Select;
use L4p1n\Form\Element\Submit;
use L4p1n\Form\Element\Text;
use L4p1n\Form\Element\Textarea;
use utilphp\util;

/**
 * Class Form
 * @package L4p1n\Form
 */
class Form {
	/**
	 *
	 */
	const FORM_BUILDING_MODE_PLAIN = 'plain';
	/**
	 *
	 */
	const FORM_BUILDING_MODE_BOOTSTRAP = 'bootstrap';
	/**
	 *
	 */
	const FORM_BUILDING_MODE_FOUNDATION = 'foundation';

	/**
	 * @var string
	 */
	protected static $form_building_mode = 'plain';
	/**
	 * @var
	 */
	private $target;
	/**
	 * @var array
	 */
	private $elements = [];
	/**
	 * @var array
	 */
	private $errors;

	/**
	 * @param $target
	 * @param array $errors
	 */
	public function __construct($target, array $errors = []){
		$this->target = $target;
		$this->errors = $errors;
	}

	/**
	 * @param $name
	 * @param $label
	 * @return Text
	 */
	public function text($name, $label){
		$element = new Text($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	/**
	 * @param $name
	 * @param $label
	 * @return Password
	 */
	public function password($name, $label){
		$element = new Password($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	/**
	 * @param $name
	 * @param $label
	 * @return Email
	 */
	public function email($name, $label){
		$element = new Email($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	/**
	 * @param $name
	 * @param $label
	 * @return Textarea
	 */
	public function textarea($name, $label){
		$element = new Textarea($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	/**
	 * @param $name
	 * @param $label
	 * @return Number
	 */
	public function int($name, $label){
		$element = new Number($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	public function number($name, $label){
		return $this->int($name, $label);
	}

	public function select($name, $label){
		$element = new Select($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	public function submit($name, $label){
		$element = new Submit($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	public function hidden($name, $label){
		$element = new Hidden($name, $label);
		$this->elements[] = $element;
		return $element;
	}

	/**
	 * @return string
	 */
	public function render(){
		$return = $this->getOpeningTag();
		/** @var BaseElement $element */
		foreach($this->elements as $element){
			$element->setError($this->getErrorFor($element->getName()));

			if(isset($_POST[$element->getName()])){
				$element->value($_POST[$element->getName()]);
			}

			if(self::$form_building_mode == self::FORM_BUILDING_MODE_BOOTSTRAP){
				$has_fixes = ($element->getPrefix() || $element->getPostfix());

				$return .= '<div class="form-group'. ($element->hasError() ? ' has-error' : '') .'">';
				$return .= $element->render(self::FORM_BUILDING_MODE_BOOTSTRAP, BaseElement::FORM_ELEMENT_RENDER_LABEL);
				if($has_fixes){
					$return .= '<div class="input-group">';

					if($element->getPrefix()){
						$return .= $element->buildPrefix(self::FORM_BUILDING_MODE_BOOTSTRAP);
					}
				}

				$return .= $element->render(self::FORM_BUILDING_MODE_BOOTSTRAP, BaseElement::FORM_ELEMENT_RENDER_INPUT);

				if($has_fixes){
					if($element->getPostfix()){
						$return .= $element->buildPostfix(self::FORM_BUILDING_MODE_BOOTSTRAP);
					}

					$return .= '</div>';
				}
				if($element->hasError()){
					$return .= '<p class="help-block">'. $element->getError() .'</p>';
				}
				$return .= '</div>';
			}elseif(self::$form_building_mode == self::FORM_BUILDING_MODE_FOUNDATION){
				// TODO: Foundation form building mode
			}elseif(self::$form_building_mode == self::FORM_BUILDING_MODE_PLAIN){
				$return .= '<p>';
				$return .= $element->render(Form::FORM_BUILDING_MODE_PLAIN, BaseElement::FORM_ELEMENT_RENDER_LABEL);
				$return .= $element->render(Form::FORM_BUILDING_MODE_PLAIN, BaseElement::FORM_ELEMENT_RENDER_INPUT);
				$return .= '</p>';
			}
		}
		$return .= $this->getClosingTag();

		return $return;
	}

	// @codeCoverageIgnoreStart
	/**
	 * @param $mode
	 */
	public static function setFormBuildingMode($mode){

		if(!in_array($mode, [self::FORM_BUILDING_MODE_BOOTSTRAP, self::FORM_BUILDING_MODE_FOUNDATION, self::FORM_BUILDING_MODE_PLAIN])){
			throw new \InvalidArgumentException('Set form building mode is not valid.');
		}

		self::$form_building_mode = $mode;
	}
	// @codeCoverageIgnoreEnd

	/**
	 * @return string
	 */
	protected function getOpeningTag(){
		$return = '<form method="post" action="' . $this->target . '">';

		return $return;
	}

	/**
	 * @return string
	 */
	protected function getClosingTag(){
		return '</form>';
	}

	/**
	 * @param $name
	 * @return null
	 */
	protected function getErrorFor($name){
		if(!empty($this->errors[$name])){
			if(is_array($this->errors[$name])){
				return util::array_first($this->errors[$name]);
			}

			return $this->errors[$name];
		}

		return null;
	}
}
