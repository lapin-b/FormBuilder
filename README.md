# PHP Form builder
This is a form builder with errors and auto form refilling.

# Installation
## With Composer
Run `composer install "l4p1n\form-builder"` and wait for the end of the process

## Manually
- Grab the [latest zip archive](https://gitlab.com/lapin-b/FormBuilder/repository/archive.zip?ref=master)
- Grab the [utilphp library](https://github.com/brandonwamboldt/utilphp/)
- Install where you want
- Don't forget to include utilphp before this library :sunglasses:

``` php
<?php
require('path/to/utilphp');
require('path/to/form/builder/load.php');
?>
```

- You're done

# Configuration
The default configuration is set to `Form::FORM_BUILDING_MODE_PLAIN`.
You can change that in one line with
```php
<?php
Form::setFormBuildingMode(
    Form::FORM_BUILDING_MODE_PLAIN |
    Form::BUILDING_MODE_BOOTSTRAP|
    Form::FORM_BUILDING_MODE_FOUNDATION
)
?>
```
Note: You can't combine all three.
Note 2: Foundation building mode is incomplete for the moment.

# Examples of use
_TODO: Add examples_
