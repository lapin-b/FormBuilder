<?php

namespace L4p1n\tests;


use L4p1n\Form\Element\Text;

class BaseElementTest extends \PHPUnit_Framework_TestCase{
	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$methods = [
			'attribute',
			'value',
			'placeholder',
			'f_class',
			'required',
			'maxLength',
			'noAutocomplete',
			'autofocus',
			'readonly',
			'pattern',
			'noSpellcheck',
			'id'
		];

		$element = new Text(null, null);
		$r_element = new \ReflectionObject($element);

		foreach($methods as $method){
			$r_method = $r_element->getMethod($method);
			$reqParams = $r_method->getNumberOfRequiredParameters();
			$name = $r_method->getName();
			$actual = call_user_func_array([$element, $name], $this->randomArray($reqParams));

			$this->assertInstanceOf(Text::class, $actual, "Method $name does not return an instance of " . Text::class);
		}

		$this->assertInstanceOf(Text::class, $element->attribute('name', 'value'));
	}

	private function randomArray($count){
		$return = [];
		for($i = 0; $i < $count; $i++){
			$return[] = uniqid(mt_rand(0, mt_getrandmax()));
		}

		return $return;
	}
}
