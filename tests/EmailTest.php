<?php

namespace L4p1n\tests;

use L4p1n\Form\Element\Email;
use L4p1n\Form\Form;

/*
 * @coversDefaultClass Email<extended>
 */
class EmailTest extends \PHPUnit_Framework_TestCase {

	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Email(null, null);
		$this->assertInstanceOf(Email::class, $element->attribute('name', 'value'));
	}

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Email(null, null);
		$this->assertInstanceOf(Email::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Email::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Email('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Email::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Email('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Email::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label class="control-label">label</label>', $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Email('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Email::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testPlainInputRendering(){
		$expected = '<input type="email" name="name"/>';
		$element = new Email('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Email::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<input type="email" name="name" class="form-control"/>';
		$element = new Email('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Email::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<input type="email" name="name" class="class form-control"/>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Email::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Email('name', 'label'))->render('incorrect', 'something'));
	}
}
