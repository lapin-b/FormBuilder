<?php

namespace L4p1n\tests;


use L4p1n\Form\Element\BaseElement;
use L4p1n\Form\Element\Email;
use L4p1n\Form\Element\Hidden;
use L4p1n\Form\Element\Number;
use L4p1n\Form\Element\Password;
use L4p1n\Form\Element\Select;
use L4p1n\Form\Element\Submit;
use L4p1n\Form\Element\Text;
use L4p1n\Form\Element\Textarea;
use L4p1n\Form\Form;

class FormTest extends \PHPUnit_Framework_TestCase {
	public function testItShouldReturnATextElement(){
		$object = $this->getObject('text');
		$this->assertInstanceOf(Text::class, $object);
	}

	public function testItShouldReturnAPasswordElement(){
		$object = $this->getObject('password');
		$this->assertInstanceOf(Password::class, $object);
	}

	public function testItShouldReturnAHiddenElement(){
		$object = $this->getObject('hidden');
		$this->assertInstanceOf(Hidden::class, $object);
	}

	public function testItShouldReturnANumberElement(){
		$object = $this->getObject('number');
		$this->assertInstanceOf(Number::class, $object);

		$object = $this->getObject('int');
		$this->assertInstanceOf(Number::class, $object);
	}

	public function testItShouldReturnAnEmailElement(){
		$object = $this->getObject('email');
		$this->assertInstanceOf(Email::class, $object);
	}

	public function testItShouldReturnAnTextareaElement(){
		$object = $this->getObject('textarea');
		$this->assertInstanceOf(Textarea::class, $object);
	}

	public function testItShouldReturnASubmitElement(){
		$object = $this->getObject('submit');
		$this->assertInstanceOf(Submit::class, $object);
	}

	public function testItShouldReturnASelectElement(){
		$object = $this->getObject('select');
		$this->assertInstanceOf(Select::class, $object);
	}

	public function testItShouldRenderSomething(){
		$form = new Form('target');
		$form->text('name', 'label');
		$actual = $form->render();

		$this->assertNotNull($actual);
	}

	public function testGetEmptyForm(){
		$expected = '<form method="post" action="target"></form>';
		$form = new Form('target');
		$actual = $form->render();

		$this->assertEquals($expected, $actual);
	}

	public function testGetFormWithInputInPlainMode(){
		$expected = '<form method="post" action="target">'
			. '<p>'
			. '<label>label</label>'
			. '<input type="text" name="name"/>'
			. '</p>'
			. '</form>';
		$form = new Form('target');
		$form->text('name', 'label');

		$this->assertEquals($expected, $form->render());
	}

	public function testGetFormInBootstrapMode(){
		$expected = '<form method="post" action="target">'
			. '<div class="form-group">'
			. '<label class="control-label">label</label>'
			. '<input type="text" name="name" class="form-control"/>'
			. '</div>'
			. '</form>';

		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_BOOTSTRAP);
		$form = new Form('target');
		$form->text('name', 'label');

		$this->assertEquals($expected, $form->render());
		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_PLAIN);
	}

	public function testGetFormInBootstrapModeWithPrefixAndSuffix(){
		$expected = '<form method="post" action="target">'
			. '<div class="form-group">'
			. '<label class="control-label">label</label>'
			. '<div class="input-group">'
			. '<div class="input-group-addon">prefix</div>'
			. '<input type="text" name="name" class="form-control"/>'
			. '<div class="input-group-addon">postfix</div>'
			. '</div>'
			. '</div>'
			. '</form>';

		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_BOOTSTRAP);
		$form = new Form('target');
		$form->text('name', 'label')->prefix('prefix')->postfix('postfix');

		$this->assertEquals($expected, $form->render());
		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_PLAIN);
	}

	public function testGetFormInBootstrapModeWithError(){
		$expected = '<form method="post" action="target">'
			. '<div class="form-group has-error">'
			. '<label class="control-label">label</label>'
			. '<input type="text" name="name" class="form-control"/>'
			. '<p class="help-block">test</p>'
			. '</div>'
			. '</form>';

		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_BOOTSTRAP);
		$form = new Form('target', ['name' => 'test']);
		$form->text('name', 'label');

		$this->assertEquals($expected, $form->render());
		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_PLAIN);
	}

	public function testGetFormInBootstrapModeWithErrorsAndValues(){
		$expected = '<form method="post" action="target">'
			. '<div class="form-group has-error">'
			. '<label class="control-label">label</label>'
			. '<input type="text" name="name" value="test" class="form-control"/>'
			. '<p class="help-block">test</p>'
			. '</div>'
			. '</form>';

		$_POST['name'] = 'test';

		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_BOOTSTRAP);
		$form = new Form('target', ['name' => 'test']);
		$form->text('name', 'label');

		$this->assertEquals($expected, $form->render());
		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_PLAIN);
		$_POST = [];
	}

	public function testGetFormInBootstrapModeWithoutErrorsWithValues(){
		$expected = '<form method="post" action="target">'
			. '<div class="form-group">'
			. '<label class="control-label">label</label>'
			. '<input type="text" name="name" value="test" class="form-control"/>'
			. '</div>'
			. '</form>';

		$_POST['name'] = 'test';

		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_BOOTSTRAP);
		$form = new Form('target');
		$form->text('name', 'label');

		$this->assertEquals($expected, $form->render());
		Form::setFormBuildingMode(Form::FORM_BUILDING_MODE_PLAIN);
		$_POST = [];
	}

	public function testGetErrorForElementWithMultiDimensionalArray(){
		$expected = 'test';

		$errors = ['username' => ['test']];
		$form = new Form(null, $errors);
		$r_form = new \ReflectionObject($form);

		$r_method = $r_form->getMethod('getErrorFor');
		$r_method->setAccessible(true);
		$actual = $r_method->invoke($form, 'username');

		$this->assertEquals($expected, $actual);
	}

	/**
	 * @return BaseElement
	 */
	protected function getObject($method){
		if(method_exists(new Form(null), $method)){
			return (new Form('target'))->$method('name', 'label');
		}else{
			return null;
		}
	}
}
