<?php
namespace L4p1n\tests;


use L4p1n\Form\Element\Hidden;
use L4p1n\Form\Form;

class HiddenTest extends \PHPUnit_Framework_TestCase{
	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Hidden(null, null);
		$this->assertInstanceOf(Hidden::class, $element->attribute('name', 'value'));
	}

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Hidden(null, null);
		$this->assertInstanceOf(Hidden::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Hidden::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Hidden('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Hidden::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals(null, $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Hidden('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Hidden::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals(null, $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Hidden('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Hidden::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals(null, $label);
	}

	public function testPlainInputRendering(){
		$expected = '<input type="hidden" name="name" value="label"/>';
		$element = new Hidden('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Hidden::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<input type="hidden" name="name" value="label"/>';
		$element = new Hidden('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Hidden::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<input type="hidden" name="name" value="label" class="class"/>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Hidden::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Hidden('name', 'label'))->render('incorrect', 'something'));
	}
}
