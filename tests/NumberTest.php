<?php

namespace L4p1n\tests;

use L4p1n\Form\Element\Number;
use L4p1n\Form\Element\Password;
use L4p1n\Form\Form;

/*
 * @coversDefaultClass Password<extended>
 */
class NumberTest extends \PHPUnit_Framework_TestCase {

	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Number(null, null);
		$this->assertInstanceOf(Number::class, $element->attribute('name', 'value'));
	}

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Number(null, null);
		$this->assertInstanceOf(Number::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Number::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Number('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Number::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Number('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Number::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label class="control-label">label</label>', $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Number('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Number::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testPlainInputRendering(){
		$expected = '<input type="number" name="name"/>';
		$element = new Number('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Number::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<input type="number" name="name" class="form-control"/>';
		$element = new Number('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Number::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<input type="number" name="name" class="class form-control"/>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Number::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Number('name', 'label'))->render('incorrect', 'something'));
	}
}
