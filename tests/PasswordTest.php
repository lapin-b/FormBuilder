<?php

namespace L4p1n\tests;

use L4p1n\Form\Element\Password;
use L4p1n\Form\Form;

/*
 * @coversDefaultClass Password<extended>
 */
class PasswordTest extends \PHPUnit_Framework_TestCase {

	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Password(null, null);
		$this->assertInstanceOf(Password::class, $element->attribute('name', 'value'));
	}

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Password(null, null);
		$this->assertInstanceOf(Password::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Password::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Password('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Password::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Password('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Password::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label class="control-label">label</label>', $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Password('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Password::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testPlainInputRendering(){
		$expected = '<input type="password" name="name"/>';
		$element = new Password('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Password::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<input type="password" name="name" class="form-control"/>';
		$element = new Password('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Password::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<input type="password" name="name" class="class form-control"/>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Password::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Password('name', 'label'))->render('incorrect', 'something'));
	}
}
