<?php

namespace L4p1n\tests;


use InvalidArgumentException;
use L4p1n\Form\Element\Select;
use L4p1n\Form\Form;

class SelectTest extends \PHPUnit_Framework_TestCase{
	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Select(null, null);
		$this->assertInstanceOf(Select::class, $element->attribute('name', 'value'));
		$this->assertInstanceOf(Select::class, $element->options(['saucisse']));
	}

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Select(null, null);
		$this->assertInstanceOf(Select::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Select::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Select('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Select::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Select('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Select::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label class="control-label">label</label>', $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Select('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Select::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testPlainInputRendering(){
		$expected = '<select name="name"><option>Hello</option></select>';
		$element = new Select('name', 'label');
		$element->options(['Hello']);
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Select::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<select name="name" class="form-control"><option>Hello</option></select>';
		$element = new Select('name', 'label');
		$element->options(['Hello']);
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Select::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<select name="name" class="class form-control"><option>Hello</option></select>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Select::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Select('name', 'label'))->render('incorrect', 'something'));
	}

	public function testRenderShouldThrowAnExceptionWhenOptionsIsEmpty(){
		$element = new Select('name', 'label');
		try{
			$element->render(Form::FORM_BUILDING_MODE_PLAIN, Select::FORM_ELEMENT_RENDER_INPUT);
			$this->fail('No exceptions thrown');
		}catch (InvalidArgumentException $e){
			$this->assertInstanceOf('\InvalidArgumentException', $e);
		}
	}
}
