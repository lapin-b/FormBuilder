<?php

namespace L4p1n\tests;

use L4p1n\Form\Element\BaseElement;
use L4p1n\Form\Element\Submit;
use L4p1n\Form\Form;

class SubmitTest extends \PHPUnit_Framework_TestCase{

	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Submit(null, null);
		$this->assertInstanceOf(Submit::class, $element->attribute('name', 'value'));
	}

	public function testRenderingWithBootstrap(){
		$element = new Submit('sub', 'sub');
		$expected = '<button name="sub" class="btn btn-primary">sub</button>';
		$result = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, BaseElement::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $result);
	}

	public function testRenderingInPlainMode(){
		$element = new Submit('sub', 'sub');
		$expected = '<input type="submit" name="sub" value="sub"/>';
		$result = $element->render(Form::FORM_BUILDING_MODE_PLAIN, BaseElement::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $result);
	}

	public function testShouldRenderNothingIfLabelIsRequested(){
		$element = new Submit('sub', 'sub');
		$expected = null;
		$result = $element->render(Form::FORM_BUILDING_MODE_PLAIN, BaseElement::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals($expected, $result);
	}

	public function testShouldRenderNothingIfInvalidMode(){
		$element = new Submit('sub', 'sub');
		$expected = null;
		$result = $element->render('invalid', BaseElement::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $result);
	}
}
