<?php

namespace L4p1n\tests;

use L4p1n\Form\Element\Text;
use L4p1n\Form\Form;

/*
 * @coversDefaultClass Text<extended>
 */
class TextTest extends \PHPUnit_Framework_TestCase {

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Text(null, null);
		$this->assertInstanceOf(Text::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Text::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Text('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Text::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Text('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Text::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label class="control-label">label</label>', $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Text('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Text::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testPlainInputRendering(){
		$expected = '<input type="text" name="name"/>';
		$element = new Text('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Text::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<input type="text" name="name" class="form-control"/>';
		$element = new Text('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Text::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<input type="text" name="name" class="class form-control"/>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Text::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Text('name', 'label'))->render('incorrect', 'something'));
	}

	public function testBuildPrefixBootstrapMode(){
		$o = new Text('name', 'label');
		$o->prefix('prefix')->postfix('postfix');

		$expected = '<div class="input-group-addon">prefix</div>';
		$this->assertEquals($expected, $o->buildPrefix(Form::FORM_BUILDING_MODE_BOOTSTRAP));
	}

	public function testBuildPostfixBootstrapMode(){
		$o = new Text('name', 'label');
		$o->prefix('prefix')->postfix('postfix');

		$expected = '<div class="input-group-addon">postfix</div>';
		$this->assertEquals($expected, $o->buildPostfix(Form::FORM_BUILDING_MODE_BOOTSTRAP));
	}

	public function testBuildPrefixWithInvalidMode(){
		$o = new Text('name', 'label');
		$o->prefix('prefix')->postfix('postfix');

		$this->assertNull($o->buildPrefix('invalid'));
	}

	public function testBuildPostfixWithInvalidMode(){
		$o = new Text('name', 'label');
		$o->prefix('prefix')->postfix('postfix');

		$this->assertNull($o->buildPostfix('invalid'));
	}
}
