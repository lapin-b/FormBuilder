<?php

namespace L4p1n\tests;


use L4p1n\Form\Element\Textarea;
use L4p1n\Form\Form;

class TextareaTest extends \PHPUnit_Framework_TestCase {
	public function testShouldReturnEverytimeAnInstanceOfTheClass(){
		$element = new Textarea(null, null);
		$this->assertInstanceOf(Textarea::class, $element->attribute('name', 'value'));
	}

	public function testPrefixAndPostfixShouldReturnAnInstanceOfTheObject(){
		$element = new Textarea(null, null);
		$this->assertInstanceOf(Textarea::class, $element->prefix('prefix'));
		$this->assertInstanceOf(Textarea::class, $element->postfix('postfix'));
	}

	public function testPlainLabelRendering(){
		$test = new Textarea('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_PLAIN, Textarea::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testLabelRenderingWithBootstrap(){
		$test = new Textarea('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Textarea::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label class="control-label">label</label>', $label);
	}

	public function testLabelRenderingWithFoundation(){
		$test = new Textarea('name', 'label');
		$label = $test->render(Form::FORM_BUILDING_MODE_FOUNDATION, Textarea::FORM_ELEMENT_RENDER_LABEL);

		$this->assertEquals('<label>label</label>', $label);
	}

	public function testPlainInputRendering(){
		$expected = '<textarea name="name"></textarea>';
		$element = new Textarea('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_PLAIN, Textarea::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);
	}

	public function testBootstrapInputRendering(){
		$expected = '<textarea name="name" class="form-control"></textarea>';
		$element = new Textarea('name', 'label');
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Textarea::FORM_ELEMENT_RENDER_INPUT);

		$this->assertEquals($expected, $actual);

		$element->f_class('class');
		$expected = '<textarea name="name" class="class form-control"></textarea>';
		$actual = $element->render(Form::FORM_BUILDING_MODE_BOOTSTRAP, Textarea::FORM_ELEMENT_RENDER_INPUT);
		$this->assertEquals($expected, $actual);
	}

	public function testShouldReturnNullIfIncorrectMode(){
		$this->assertNull((new Textarea('name', 'label'))->render('incorrect', 'something'));
	}
}
